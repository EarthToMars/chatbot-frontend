import React, { useState, useEffect } from "react";
import { Button, Table } from "reactstrap";
import axios from "axios";
import ChatMessages from "../components/chatMessages";
const BASE_URL = "http://localhost:8080";
const Home = () => {
  const [chats, setChats] = useState([]);
  const [activeChat, setActiveChat] = useState(undefined);
  const [chatMessages, setChatMessages] = useState([]);

  useEffect(() => {
    getAllChats();
  }, []);

  const getAllChats = () => {
    axios
      .get(`${BASE_URL}/chat`)
      .then((response) => {
        if (response?.data?.content?.length > 0) {
          setChats([...response.data.content]);
          setActiveChat(response.data.content[0]);
        } else {
          setChats([]);
          setActiveChat(undefined);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  useEffect(() => {
    if (activeChat !== undefined) {
      openChat(activeChat.id);
    }
  }, [activeChat]);

  const openChat = (chatId) => {
    axios
      .get(
        `${BASE_URL}/chat/getChatMessages?chatId=${chatId}`
      )
      .then((response) => {
        if (response?.data?.content?.length) {
          setChatMessages([...response.data.content]);
        } else {
          setChatMessages([]);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const sendResponse = (reply, chatId) =>{
    axios
      .post(
        `${BASE_URL}/chat?chatId=${chatId}`,{
          reply
        }
      )
      .then((response) => {
        console.log("Reply sent");
        openChat(chatId);
      })
      .catch((err) => {
        console.log(err);
      });
  };


  return (
    <div>
      <div className="row m-1">
        <div className="col-6">
          <h2>CHAT BOT</h2>
        </div>
        <div className="col-6 mr-0">
          <Button
            color="primary"
            onClick={(e) => {
              e.preventDefault();
              getAllChats();
            }}
          >
            Reload Chats
          </Button>
        </div>
      </div>

      <Table>
        <thead>
          <tr>
            <th>#</th>
            <th>Customer Name</th>
            <th>Employee Name</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {chats.map((chat, index) => (
            <tr key={index}>
              <th scope="row">{index + 1}</th>
              <td>{`${chat.customerFirstName} ${chat.customerLastName}`}</td>
              <td>{`${chat.employeeFirstName} ${chat.employeeLastName}`}</td>
              <td>
                <Button
                  color="primary"
                  disabled={activeChat?.id === chat.id}
                  onClick={() => {
                    setActiveChat({...chat})
                  }}
                >
                  Open Chat
                </Button>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
      {chatMessages?.length > 0 && <ChatMessages chatMessages={chatMessages} sendReply={sendResponse} chatId={activeChat.id} refreshMessages={()=>{openChat(activeChat.id)}} />}
    </div>
  );
};

export default Home;
