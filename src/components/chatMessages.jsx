import React, { useState } from "react";
import { Button } from "reactstrap";

const ChatMessages = ({
  chatMessages = [],
  chatId = null,
  refreshMessages = () => console.log("Default refresh messages"),
  sendReply = () => console.log("Default reply sent") 
}) => {
  const [reply, setReply] = useState("");
  return (
    <div className="row m-1">
      <div className="col-6">
        <Button
          color="primary"
          onClick={() => {
            refreshMessages();
          }}
        >
          Reload Messages
        </Button>
      </div>
      <div className="m-1">
        {chatMessages?.map((message, index) => (
          <p
            style={{
              whiteSpace : "pre-wrap",
              textAlign: `${message.chatType === "SEND" ? "left" : "right"}`,
            }}
            key={index}
          >{`${message.message}`}</p>
        ))}
      </div>
      <div className="row">
        <div className="col-10">
          <input type="text" style={{width: '100%'}} onChange={(e) => setReply(e.target.value)} value={reply}/>
        </div>
        <div className="col-2"><Button
          color="primary"
          onClick={() => {
            sendReply(reply,chatId);
            setReply("");
          }}
        >
          Reply
        </Button></div>
      </div>
    </div>
  );
};



export default ChatMessages;
