import Home from "./pages/home";

function App() {
  return (
    <div className="chatbot-app">
      <Home/>
    </div>
  );
}

export default App;
